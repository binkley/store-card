Apple Wallet Store Card Generator
=================================

Generate Migros Cumulus Store Card with location relevance.

How To
------

1. Install prerequisites: `pip install -r requirements.txt`

2. Download [signpass sources](https://developer.apple.com/services-account/download?path=/iOS/Wallet_Support_Materials/WalletCompanionFiles.zip) and build it.

3. [Create your own pass type ID](https://developer.apple.com/account/ios/identifier/passTypeId)

4. [Create a signing certificate](https://developer.apple.com/account/ios/certificate)

5. Create and edit your configuration: `cp config-sample.json config.json`

6. Run `python store-card.py` to build the pass in the `build` directory.

7. AirDrop the `.pkpass` file to your iPhone or iPod touch.

When you get near a supported store the pass should appear on your lock screen.

Apple Documentation
-------------------

- [Wallet Developer Guide](https://developer.apple.com/library/content/documentation/UserExperience/Conceptual/PassKit_PG/index.html)

Thanks
------

- Lukas Kollmer for [inspiration](https://github.com/lukaskollmer/passcard).
- Michael Mulqueen and HUDORA for the [EAN-13 bar code generator](https://github.com/mmulqueen/pyStrich).
- [NumeriCube](http://selfpass.net) for the idea to handle unsupported bar codes using the strip image.
