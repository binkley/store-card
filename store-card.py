#!/usr/bin/env python
# vim: set fileencoding=utf-8:

# Generate Migros Cumulus Store Card with location relevance

import json
import os
import shutil
import subprocess
from pystrich.ean13 import EAN13Encoder
from PIL import Image
from StringIO import StringIO
from os.path import exists

FILTER = Image.ANTIALIAS

buildDir = 'build'
passDir = buildDir + '/pass'

def centeredImage(width, height, image, mode='RGBA'):
    resultImage = Image.new(mode, (width, height), 'white')
    x = (width - image.size[0])/2
    y = (height - image.size[1])/2
    resultImage.paste(image, (x, y))
    return resultImage

def cleanPass():
    if exists(passDir):
        shutil.rmtree(passDir)
    os.makedirs(passDir)

def createPassImages(cardCode):
    # storeCard: logo, icon, strip
    iconImage = Image.open('branding/migros/icon.png')
    if float(iconImage.size[0]) / float(iconImage.size[1]) != 1.0:
        raise Exception('%s must be square' % iconImage.filename)

    orgLogoImage = Image.open('branding/migros/logo.png')
    width = 0.8*160*6
    height = width * orgLogoImage.size[1] / orgLogoImage.size[0]
    sizedLogoImage = orgLogoImage.resize((int(width), int(height)), FILTER)
    logoImage = centeredImage(160*6, 50*6, sizedLogoImage)

    encoder = EAN13Encoder(cardCode)
    for scale in [1, 2, 3]:
        suffix = '' if scale == 1 else '@%dx' % scale

        codePng = encoder.get_imagedata(bar_width=3*scale)
        codeImage = Image.open(StringIO(codePng))

        # crop off barcode text
        cropImage = codeImage.crop((0, 13*scale, codeImage.size[0], codeImage.size[1]-(13+48)*scale))

        # The strip image (strip.png) is displayed behind the primary fields.
        # On iPhone 6 and 6 Plus The allotted space is 375 x 98 points for event tickets, 375 x 144 points for gift cards and coupons, and 375 x 123 in all other cases.
        # On prior hardware The allotted space is 320 x 84 points for event tickets, 320 x 110 points for other pass styles with a square barcode on devices with 3.5 inch screens, and 320 x 123 in all other cases.
        width = 375 * scale
        height = 123 * scale
        stripImage = centeredImage(width, height, cropImage, mode='L')

        filename = '%s/strip%s.png' % (passDir, suffix)
        stripImage.save(filename, 'PNG')

        # The icon (icon.png) is displayed when a pass is shown on the lock screen and by apps such as Mail when showing a pass attached to an email. The icon should measure 29 x 29 points.
        iconImage.resize((29*scale, 29*scale), FILTER).save('%s/icon%s.png' % (passDir, suffix))

        # The logo image (logo.png) is displayed in the top left corner of the pass, next to the logo text. The allotted space is 160 x 50 points; in most cases it should be narrower.
        logoImage.resize((160*scale, 50*scale), FILTER).save('%s/logo%s.png' % (passDir, suffix))

def createPassInfo(config):
    cardCode = config['cardCode']
    firstName = config['firstName']
    lastName = config['lastName']
    passTypeIdentifier = config['passTypeIdentifier']
    teamIdentifier = config['teamIdentifier']

    # https://developer.apple.com/library/content/documentation/UserExperience/Conceptual/PassKit_PG/index.html
    # https://developer.apple.com/library/content/documentation/UserExperience/Reference/PassKit_Bundle/Chapters/Introduction.html

    passInfo = {
        "formatVersion" : 1,
        "passTypeIdentifier" : passTypeIdentifier,
        "serialNumber" : cardCode,
        "teamIdentifier" : teamIdentifier,
        "organizationName" : "Migros",
        "description" : "Migros Cumulus",
        "foregroundColor" : "rgb(252, 102, 33)",
        "backgroundColor" : "rgb(255, 255, 255)",
        "suppressStripShine": True,
        "maxDistance": 600,
        "locations": [
            {
                "latitude": 47.3740,
                "longitude": 8.6410,
                "relevantText": "Welcome to Migros Fällanden."
            },
            {
                "latitude": 47.3660,
                "longitude": 8.5533,
                "relevantText": "Welcome to Migros Kreuzplatz."
            }
        ],
        "storeCard": {
            "primaryFields" : [],
            "secondaryFields": [
                {
                    "key": "firstName",
                    "label": "First Name",
                    "textAlignment": "PKTextAlignmentLeft",
                    "value": firstName
                },
                {
                    "key": "lastName",
                    "label": "Last Name",
                    "textAlignment": "PKTextAlignmentLeft",
                    "value": lastName
                }
            ],
            "auxiliaryFields": [
                {
                    "key": "card_serial",
                    "label": "Cumulus",
                    "textAlignment": "PKTextAlignmentLeft",
                    "value": cardCode
                }
            ]
        }
    }
    open(passDir + '/pass.json', 'w').write(json.dumps(passInfo))

def signPass():
    # Uses https://developer.apple.com/services-account/download?path=/iOS/Wallet_Support_Materials/WalletCompanionFiles.zip
    subprocess.check_call(['./signpass', '-p', passDir, '-o', buildDir + '/migros.pkpass'])

def main():
    config = json.load(open('config.json'))
    cleanPass()
    createPassImages(config['cardCode'])
    createPassInfo(config)
    signPass()

main()
